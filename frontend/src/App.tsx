import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import Navbar from "./components/Navbar";
import Readme from "./pages/Readme";
import Solution from "./pages/Solution";
import "../src/styles/styles.scss";

const App = () => {
  const history = React.useMemo(createBrowserHistory, []);
  return (
    <Router history={history}>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/" exact component={Readme} />
          <Route path="/solution" exact component={Solution} />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
