import { gql } from "@apollo/client";

export const GET_SESSIONS = gql`
  query GetSessions {
    sessions {
      id
      image
      signups {
        id
      }
      startTime
      strapline
      title
    }
  }
`;

