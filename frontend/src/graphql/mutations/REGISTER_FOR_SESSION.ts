import { gql} from "@apollo/client";

export const REGISTER_FOR_SESSION = gql`
  mutation RegisterForSession($session: ID!) {
    register(sessionId: $session) {
      id
      session {
        id
        signups {
          id
        }
      }
    }
  }
`;
