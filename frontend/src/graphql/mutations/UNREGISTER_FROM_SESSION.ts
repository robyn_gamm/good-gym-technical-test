import { gql } from "@apollo/client";

export const UNREGISTER_FROM_SESSION = gql`
  mutation UnregisterFromSession($session: ID!) {
    unregister(sessionId: $session)
  }
`;