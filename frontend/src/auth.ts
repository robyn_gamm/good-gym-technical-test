import { useQuery } from "@apollo/client";
import gql from "graphql-tag";

const CURRENT_USER = gql`
  query CurrentUser {
    currentUser {
      id
      name
      avatar
    }
  }
`;

export const useAuth = () => {
  const { data } = useQuery(CURRENT_USER);
  return { user: data?.currentUser };
};
