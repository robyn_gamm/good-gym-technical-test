import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";

const http = createHttpLink({
  uri: "/graphql",
});

export const client = new ApolloClient({
  link: http,
  cache: new InMemoryCache({
    typePolicies: {
      Query: { fields: { sessions: { merge: false } } },
      Session: {
        fields: {
          signups: {
            merge: false,
          },
        },
      },
    },
  }),
  credentials: "same-origin",
});
