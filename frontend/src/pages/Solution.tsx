import React from "react";
import { useAuth } from "auth";
import { useQuery } from "@apollo/client";
import { SessionCard } from "../components/SessionCard/SessionCard";
import * as UI from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../components/FancyCard/FancyCard.module.scss";
import { GET_SESSIONS } from "graphql/queries/GET_SESSIONS";

export interface SessionsData {
  id: string;
  image: string;
  signups: { id: string }[];
  startTime: string;
  strapline: string;
  title: string;
}

export interface SessionsResult {
  sessions: Array<SessionsData>;
}

const useStyles = makeStyles((theme) => ({
  secondaryBackground: {
    backgroundColor: "#f8f9fa",
  },
}));

const Solution = () => {
  const classes = useStyles();
  const { loading, error, data } = useQuery<SessionsResult>(GET_SESSIONS);

  const { user } = useAuth();
  if (!user) return null;
  return (
    <React.Fragment>
      <UI.Container>
        <UI.Box py={6} textAlign="center">
          <h1>Upcoming Events</h1>
          <p>Meet others, get fit, and do good</p>
        </UI.Box>
      </UI.Container>

      {loading && <p>Loading sessions</p>}
      {error && (
        <p>Oops, there was a problem loading sessions. Sorry about that!</p>
      )}

      {data && (
        <UI.Box py={12} className={classes.secondaryBackground}>
          <UI.Container>
            <div className={styles.FancyCardContainer}>
              {data.sessions.map((session) => (
                <SessionCard sessionData={session} key={session.id} />
              ))}
            </div>
          </UI.Container>
        </UI.Box>
      )}
    </React.Fragment>
  );
};

export default Solution;
