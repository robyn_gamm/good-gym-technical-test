import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { SessionsData } from "pages/Solution";
import * as UI from "@material-ui/core";
import { RegisterButton } from "../RegisterButton/RegisterButton";
import { useAuth } from "auth";
import { UnregisterButton } from "components/UnregisterButton/UnregisterButton";

interface Props {
  sessionData: SessionsData;
}

export const SessionCard = ({ sessionData }: Props) => {
  const { user } = useAuth();
  const signUps = sessionData.signups;
  const userIsSignedUp = signUps.some((signUp) => {
    return signUp.id === user.id;
  });
  const signupCount = sessionData.signups.length;
  const useStyles = makeStyles({
    card: {
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
    },
    media: {
      height: 140,
    },
    actions: {
      marginTop: "auto",
      justifyContent: "space-between",
    },
  });

  const classes = useStyles();
  return (
    <UI.Card className={classes.card}>
      <UI.CardActionArea>
        <UI.CardMedia
          className={classes.media}
          image={sessionData.image}
          title=""
        />
        <UI.CardContent>
          <UI.Typography
            gutterBottom
            variant="overline"
            display="block"
            color="textSecondary"
          >
            {sessionData.startTime}
          </UI.Typography>
          <UI.Typography gutterBottom variant="h5" component="h2">
            {sessionData.title}
          </UI.Typography>

          <UI.Typography gutterBottom variant="body2" component="p">
            {sessionData.strapline}
          </UI.Typography>
        </UI.CardContent>
      </UI.CardActionArea>
      <UI.CardActions className={classes.actions}>
        {userIsSignedUp && signupCount > 1 && (
          <>
            <UI.Typography variant="caption" color="textSecondary">
              You're going with {signupCount - 1} others!
            </UI.Typography>
            <UnregisterButton sessionId={sessionData.id} />
          </>
        )}
        {userIsSignedUp && signupCount <= 1 && (
          <>
            <UI.Typography variant="caption" color="textSecondary">
              You're the only runner
            </UI.Typography>
            <UnregisterButton sessionId={sessionData.id} />
          </>
        )}
        {!userIsSignedUp && signupCount > 0 && (
          <>
            <UI.Typography variant="caption" color="textSecondary">
              Join {signupCount} other runners
            </UI.Typography>
            <RegisterButton sessionId={sessionData.id} />
          </>
        )}
        {!userIsSignedUp && signupCount === 0 && (
          <>
            <UI.Typography variant="caption" color="textSecondary">
              Be the first to register
            </UI.Typography>
            <RegisterButton sessionId={sessionData.id} />
          </>
        )}
      </UI.CardActions>
    </UI.Card>
  );
};
