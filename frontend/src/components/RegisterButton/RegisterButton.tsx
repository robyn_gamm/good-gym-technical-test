import React from "react";
import { useMutation } from "@apollo/client";
import * as UI from "@material-ui/core";
import { REGISTER_FOR_SESSION } from "../../graphql/mutations/REGISTER_FOR_SESSION";

interface Props {
  sessionId: string;
}

export const RegisterButton = ({ sessionId }: Props) => {
  const [registerForSession, { loading, error }] =
    useMutation(REGISTER_FOR_SESSION);

  if (loading)
    return (
      <UI.Button
        variant="outlined"
        color="primary"
        onClick={() =>
          registerForSession({ variables: { session: sessionId } })
        }
      >
        SIGNING UP...
      </UI.Button>
    );
  if (error)
    return (
      <p>
        COULD NOT SIGN YOU UP
        {error.message}
      </p>
    );

  return (
    <UI.Button
      variant="outlined"
      color="primary"
      onClick={() => registerForSession({ variables: { session: sessionId } })}
    >
      SIGN UP NOW
    </UI.Button>
  );
};
