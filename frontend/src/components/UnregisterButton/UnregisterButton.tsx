import React from "react";
import { useMutation } from "@apollo/client";
import * as UI from "@material-ui/core";
import { UNREGISTER_FROM_SESSION } from "graphql/mutations/UNREGISTER_FROM_SESSION";

interface Props {
  sessionId: string;
}

export const UnregisterButton = ({ sessionId }: Props) => {
  const [unregisterFromSession, { loading, error }] = useMutation(
    UNREGISTER_FROM_SESSION,
    {
      refetchQueries: ["GetSessions"],
    }
  );

  if (loading)
    return (
      <UI.Button
        onClick={() =>
          unregisterFromSession({ variables: { session: sessionId } })
        }
      >
        UNREGISTERING...
      </UI.Button>
    );
  if (error)
    return (
      <p>
        COULD NOT UNREGISTER
        {error.message}
      </p>
    );

  return (
    <UI.Button
      onClick={() =>
        unregisterFromSession({ variables: { session: sessionId } })
      }
    >
      UNREGISTER
    </UI.Button>
  );
};
