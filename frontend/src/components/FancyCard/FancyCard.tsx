import React from "react";
import styles from "./FancyCard.module.scss";
import { SessionsData } from "pages/Solution";

interface Props {
  sessionData: SessionsData;
}

export const FancyCard = ({ sessionData }: Props) => {
  return (
    <div className={styles.FancyCard}>
      <div
        className={styles.FancyCard_image}
        style={{ backgroundImage: `url(${sessionData.image})` }}
      >
        <div className={styles.FancyCard_tag}>REGISTERED</div>
      </div>
      <div className={styles.FancyCard_copyContainer}>
        <p className={styles.FancyCard_caption}>Tuesday, 27th of July 2021</p>
        <p className={styles.FancyCard_title}>{sessionData.title}</p>
        <p className={styles.FancyCard_description}>{sessionData.strapline}</p>
      </div>
    </div>
  );
};
